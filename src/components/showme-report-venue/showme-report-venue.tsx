import { Component, Listen } from '@stencil/core';
import { Quelly } from './Quelly';
import { QuellyPayload } from './QuellyPayload';

@Component({
  tag: 'showme-report-venue',
  styleUrl: 'showme-report-venue.css',
  shadow: true
})
export class ShowmeReportVenue {
  portRequestQueue: Quelly<QuellyPayload> = new Quelly;

  @Listen('backstagePortRequest')
  handleBackstagePortRequest(backstagePortRequest) {
    this.portRequestQueue.enqueue(backstagePortRequest);
  }
  @Listen('backstagePortReady')
  handleBackstagePortReady(backstagePortConductor) {
    this.portRequestQueue.unwind(backstagePortConductor.detail);  
  }
  
  render() {
    return (
      <div>
        <slot/>
      </div>
    );
  }
}
