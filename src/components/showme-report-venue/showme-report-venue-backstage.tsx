import { Component, Element } from '@stencil/core';

@Component({
  tag: 'showme-report-venue-backstage',
  styleUrl: 'showme-report-venue-backstage.css',
  shadow: true
})
export class ShowmeReportVenueBackstage {
  @Element() backstageElement: HTMLElement;

  // https://stackoverflow.com/a/35385518
  htmlToElement(html) {
    const template = document.createElement('template');
    html = html.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = html;
    return template.content.firstChild;
  }

  componentDidLoad() {
    window.addEventListener('message', (event) => {
      //FIXME check origin
      switch(event.data) {
        case 'CURTAIN_CALL':
          event.ports[0].onmessage = (message) => {
            const element = this.htmlToElement(message.data);
            this.backstageElement.shadowRoot.appendChild(element);
          };
          break;
        case 'DONE_CURTAIN_CALL':
          // TODO only call  READY_BACK_STAGE after compere
          // is done sending nodes AND each of the nodes has loaded.
          parent.postMessage('READY_BACK_STAGE', '*');
          break; 
        case 'PROPERTY_CONTROL_UPDATE_PORT':
          event.ports[0].onmessage = (event) => {
            const propertyControlEvent = event.data;
            const targetComponent = this.backstageElement.shadowRoot.querySelector(propertyControlEvent.targetComponentName);
            targetComponent[propertyControlEvent.targetComponentPropertyName] = propertyControlEvent.propertyValue;
          }
          event.ports[0].postMessage('READY');
          break;
      }
    });    
  }

  render() {
    return null;
  }
}