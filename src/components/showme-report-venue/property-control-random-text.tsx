import { Component, Prop, State, Event, EventEmitter } from '@stencil/core';
import { PropertyControlEvent } from './property-control-event';

@Component({
  tag: 'property-control-random-text',
  styleUrl: 'property-control-random-text.css',
  shadow: true
})
export class PropertyControlRandomText {
  timer: number;
  @Prop() textLength: number = 20;
  @Prop() timerIntervalMs: number = 1000;
  @Prop() targetComponentName: string;
  @Prop() targetPropertyName: string;
  @State() text: string;
  @Event() propertyControlUpdateEvent: EventEmitter;

  generateRandomText() {
    if (!this.textLength || this.textLength <= 0 || this.textLength > 2000) {
      return null;
    }
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (let i = 0; i < this.textLength; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }
  
  dispatchUpdateEvent() {
    if (this.targetComponentName && this.targetPropertyName) {
      const propertyControlEvent: PropertyControlEvent = new PropertyControlEvent(this.targetComponentName, this.targetPropertyName, this.text);      // TODO emit property control update event
      this.propertyControlUpdateEvent.emit(propertyControlEvent);
    }
  }

  componentDidLoad() {
    if (!this.timerIntervalMs || this.timerIntervalMs == 0 || this.timerIntervalMs > 30000) {
      this.text = this.generateRandomText();
      this.dispatchUpdateEvent();
    } else {
      this.timer = window.setInterval(() => {
        this.text = this.generateRandomText();
        this.dispatchUpdateEvent();
      }, this.timerIntervalMs);
    }
  }

  componentDidUnload() {
    window.clearInterval(this.timer);
  }

  render() {
    return (
      <div>
        {this.text}
      </div>
    );
  }
}
