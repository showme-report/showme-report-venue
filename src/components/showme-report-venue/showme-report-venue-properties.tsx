import { Component, Event, EventEmitter, Listen } from '@stencil/core';
import { PropertyControlEvent } from './property-control-event';
import { Quelly } from './Quelly';
import { QuellyPayload } from './QuellyPayload';

@Component({
  tag: 'showme-report-venue-properties',
  styleUrl: 'showme-report-venue-properties.css',
  shadow: true
})
export class ShowmeReportVenueProperties {
  messageChannel: MessageChannel = new MessageChannel();
  propertyControlUpdateEventQueue: Quelly<PropertyControlEvent> = new Quelly();
  @Event() backstagePortRequest: EventEmitter;

  @Listen('propertyControlUpdateEvent')
  handlePropertyControlUpdateEvent(propertyControlUpdateEvent) {
    this.propertyControlUpdateEventQueue.enqueue(propertyControlUpdateEvent.detail);
  }
  
  componentDidLoad() {
    this.messageChannel.port1.onmessage = (message) => {
      if (message.data == 'READY') {
        this.propertyControlUpdateEventQueue.unwind(propertyControlUpdateEvent => {
          this.messageChannel.port1.postMessage(propertyControlUpdateEvent);
        });
      }
    }
    this.backstagePortRequest.emit(new QuellyPayload('PROPERTY_CONTROL_UPDATE_PORT', this.messageChannel.port2));
  }
  render() {
    return (
      <slot/>
    );
  }
}
