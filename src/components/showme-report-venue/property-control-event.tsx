export class PropertyControlEvent {
  targetComponentName: string;
  targetComponentPropertyName: string;
  propertyValue: any;

  constructor(targetComponentName: string, targetComponentPropertyName: string, propertyValue: string) {
    this.targetComponentName = targetComponentName;
    this.targetComponentPropertyName = targetComponentPropertyName;
    this.propertyValue = propertyValue;
  }
  
}