export class Quelly<PayloadType> {
  payloads: PayloadType[] = [];
  conductor: Function = null;

  enqueue = (payload: PayloadType) => {
    if (this.conductor && this.payloads.length == 0) {
      this.conductor(payload);
    } else {
      this.payloads.unshift(payload);
    }
  }
  
  unwind = (conductor: Function) => {
    this.conductor = conductor;
    while(this.payloads.length > 0) {
      conductor(this.payloads.pop());
    }
  }
}