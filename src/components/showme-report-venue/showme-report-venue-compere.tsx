import { Component, State, Element, Event, EventEmitter } from '@stencil/core';

@Component({
  tag: 'showme-report-venue-compere',
  styleUrl: 'showme-report-venue-compere.css',
  shadow: true
})
export class ShowmeReportVenueCompere {
  @State() curtainElement: HTMLIFrameElement;
  @Event() backstagePortReady: EventEmitter;
  @Element() compereElement: HTMLElement;

  componentDidLoad() {
    this.curtainElement = this.compereElement.shadowRoot.querySelector('#curtain') as HTMLIFrameElement;
    this.curtainElement.addEventListener('load', () => {
      const channel = new MessageChannel();
      const port1 = channel.port1;
      //FIXME specify target origin
      this.curtainElement.contentWindow.postMessage('CURTAIN_CALL', '*', [channel.port2]);
      const greenroomElement = this.compereElement.shadowRoot.querySelector('slot[name=\"greenroom\"]') as HTMLSlotElement;
      greenroomElement.assignedNodes().forEach(assignedNode => {
        const assignedNodeASHTMLElement = assignedNode as HTMLElement;
        port1.postMessage(assignedNodeASHTMLElement.outerHTML);
      });
      this.curtainElement.contentWindow.postMessage('DONE_CURTAIN_CALL', '*');
      this.backstagePortReady.emit((backStagePortRequest) => {
        this.curtainElement.contentWindow.postMessage(backStagePortRequest.detail.message, '*', [backStagePortRequest.detail.data]);
      });
    });
  }

  render() {
    return (
      [
        <iframe id="curtain" 
                src="/assets/backstage.html"
                sandbox="allow-scripts allow-same-origin"
                seamless></iframe>,
        <template><slot name="greenroom"></slot></template>
      ]
    );
  }
}
