import { Config } from '@stencil/core';

export const config: Config = {
  namespace: 'showme-report-venue',
  outputTargets:[
    {
      type: 'dist'
    },
    {
      type: 'www',
      serviceWorker: null
    }
  ]
};
